﻿using System;
namespace StudyHandy
{
    public static class YandexTranslatorSettings
    {
        public static string HOST = "https://translate.yandex.net/api/v1.5/tr/translate";

        public static string API = "?key=trnsl.1.1.20190809T195956Z.2045ef05d1a69b13.6e10ed8a4026a78a46d6f46c8d592460e4d7cbce";

        public static string Lang = "&lang={0}";

        public static string TextToTranslate = "&text={0}";
    }
}

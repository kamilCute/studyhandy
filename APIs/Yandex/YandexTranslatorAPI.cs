﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace StudyHandy.Yandex.Translator
{
    internal class YandexTranslatorAPI
    {
        HttpClient client;
        string lang;

        public YandexTranslatorAPI()
        {
            client = new HttpClient();
        }

        public async Task<string> GetTranslate(string englishWord, LangCode langCode)
        {
            switch (langCode)
            {
                case LangCode.EngToRus:
                    lang = String.Format(YandexTranslatorSettings.Lang, "en-ru");
                    break;
                case LangCode.RusToEng:
                    lang = String.Format(YandexTranslatorSettings.Lang, "ru-en");
                    break;
            }

            string serializedWord = String.Format(YandexTranslatorSettings.TextToTranslate, englishWord);
            string serializedLangCode = String.Format(YandexTranslatorSettings.Lang, lang);

            string address = YandexTranslatorSettings.HOST + YandexTranslatorSettings.API + serializedLangCode + serializedWord;

            HttpResponseMessage response = await client.GetAsync(address);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                HttpContent responseContent = response.Content;

                var xml = await responseContent.ReadAsStringAsync();

                string translatedWord = RetrieveTranslateFromXML(xml);

                return translatedWord;
            }

            return "";
        }

        string RetrieveTranslateFromXML(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc.GetElementsByTagName("text")[0].InnerText;
        }
    }
    public enum LangCode
    {
        EngToRus,
        RusToEng
    }
}

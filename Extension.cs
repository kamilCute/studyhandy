﻿using System;
using UIKit;

namespace StudyHandy.Extensions
{
    public static class Extension
    {
        static string fontType = "Georgia";

        public static UIFont getRegular(float fontSize)
        {
            return UIFont.FromName(fontType, fontSize);
        }

        public static UIFont getBold(float fontSize)
        {
            return UIFont.FromName($"{fontType}-Bold", fontSize);
        }

        public static void SetMaxLenght(UITextField thisTxtField, int quantity)
        {
            thisTxtField.ShouldChangeCharacters = (textField, range, replacementString) =>
            {
                var newLength = textField.Text.Length + replacementString.Length - range.Length;
                return newLength <= quantity;
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace StudyHandy.HeaderView
{
    public class HeaderData
    {
        public string Title;

        public ExpandableHeaderView headerLink { get; set; }

        public List<SQLWordTable> wordTableList { get; set; }

        public bool expanded { get; set; }
    }
}

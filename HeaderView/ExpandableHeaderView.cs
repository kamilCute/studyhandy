﻿using System;
using CoreGraphics;
using UIKit;

namespace StudyHandy.HeaderView
{
    public class ExpandableHeaderView : UITableViewHeaderFooterView
    {
        public Action Touched;

        public UIButton imageButton = new UIButton();

        public static int headerHeight;

        public ExpandableHeaderView()
        {
            SetupGestures();

            AddSubview(imageButton);
        }

        public void setup(string title)
        {
            TextLabel.Text = title;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            int btnWidth = 15;
            int btnHeight = 15;

            imageButton.Frame = new CGRect(new CGPoint((int)UIScreen.MainScreen.Bounds.Width - 35, headerHeight / 2.7), new CGSize(btnWidth, btnHeight));

            TextLabel.TextColor = UIColor.White;
            ContentView.BackgroundColor = UIColor.DarkGray;
        }

        void SetupGestures()
        {
            AddGestureRecognizer(new UITapGestureRecognizer(tap =>
            {
                Touched.Invoke();
            }));

            imageButton.AddGestureRecognizer(new UITapGestureRecognizer(tap =>
            {
                Touched.Invoke();
            }));
        }

    }
}
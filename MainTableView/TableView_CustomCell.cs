﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace StudyHandy
{
    public class TableView_CustomCell : UITableViewCell
    {
        public UILabel textLabel, detailedTextLabel;

        int cellHeight;

        int marginX, marginY;

        public TableView_CustomCell(NSString cellId, int cellHeight) : base(UITableViewCellStyle.Default, cellId)
        {
            this.cellHeight = cellHeight;

            InitLayout();

            textLabel = new UILabel()
            {
                Font = UIFont.FromName("AppleSDGothicNeo-SemiBold", 17f),
                TextColor = UIColor.FromRGB(13, 11, 51),
                BackgroundColor = UIColor.Clear
            };

            detailedTextLabel = new UILabel()
            {
                Font = UIFont.FromName("AppleSDGothicNeo-SemiBold", 16f),
                TextColor = UIColor.FromRGB(69, 68, 68),
                BackgroundColor = UIColor.Clear
            };

            ContentView.AddSubviews(new UIView[] { textLabel, detailedTextLabel });
        }

        public override void LayoutSubviews()
        {
            ClipsToBounds = true;

            base.LayoutSubviews();

            textLabel.Frame = new CGRect(marginX, marginY, Bounds.Width, 15);
            detailedTextLabel.Frame = new CGRect(marginX, textLabel.Frame.Bottom + marginY, Bounds.Width, 15);
        }

        void InitLayout()
        {
            marginX = 15;

            marginY = cellHeight / 6;
        }
    }
}

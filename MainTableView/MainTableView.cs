using Foundation;
using System;
using UIKit;
using System.IO;
using System.Collections.Generic;
using StudyHandy.HeaderView;
using System.Linq;
using System.Drawing;

namespace StudyHandy
{
    public partial class MainTableView : UITableViewController
    {
        #region Fields

        protected readonly string wordTableDbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), SQLWordTable_DBPath.thisDBPath);

        DataBase db;

        public static List<HeaderData> headers = new List<HeaderData>();
        int headerHeight = 44;

        int cellHeight = 60;

        int currSection;

        List<SQLWordTable> wordTableList;
        List<int> IDs;

        EditWordButtonRole editWordButtonRole = EditWordButtonRole.Edit;

        bool headerLock;

        #endregion

        #region ctor

        public MainTableView(IntPtr handle) : base(handle)
        {
            headers.Add(new HeaderData { Title = "Learning now", wordTableList = new List<SQLWordTable>(), expanded = true }) ;
            headers.Add(new HeaderData { Title = "Almost finished", wordTableList=new List<SQLWordTable>(),expanded = true });
            
            GetWordsFromDatabase();
        }

        #endregion

        #region Overriden Members

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell("Reuse Identifier") as TableView_CustomCell;

            if (cell == null)
            {
                cell = new TableView_CustomCell((Foundation.NSString)"Reuse Identifier", cellHeight);
            }

            cell.textLabel.Text = headers[indexPath.Section].wordTableList[indexPath.Row].EnglishWord;

            cell.detailedTextLabel.Text = headers[indexPath.Section].wordTableList[indexPath.Row].RussianWord;

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            currSection = Convert.ToInt32(section);

            return headers[currSection].wordTableList.Count();
        }

        public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, Foundation.NSIndexPath indexPath)
        {
            switch (editingStyle)
            {
                case UITableViewCellEditingStyle.Delete:
                    if (indexPath.Section == 0)
                    {
                        db.DeleteFromDataBase(IDs[indexPath.Row]);
                    }
                    else if (indexPath.Section == 1)
                    {
                        //we increasing IDs index value by first section's words count, beacause of
                        //second secton's words Ids going after Ids of first section
                        db.DeleteFromDataBase(IDs[indexPath.Row + headers[indexPath.Section - 1].wordTableList.Count]);
                    }

                    GetWordsFromDatabase();
                    TableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
                    break;

                case UITableViewCellEditingStyle.None:
                    Console.WriteLine("CommitEditingStyle:None called");
                    break;
            }
        }

        public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
        {
            return Editing;
        }

        public override void MoveRow(UITableView tableView, NSIndexPath sourceIndexPath, NSIndexPath destinationIndexPath)
        {
            SQLWordTable item = headers[sourceIndexPath.Section].wordTableList[sourceIndexPath.Row];

            item.headerIndex = destinationIndexPath.Section;

            var deleteAt = sourceIndexPath.Row;
            var insertAt = destinationIndexPath.Row;

            // are we inserting 
            if (destinationIndexPath.Row < sourceIndexPath.Row && sourceIndexPath.Section == destinationIndexPath.Section)
            {
                // add one to where we delete, because we're increasing the index by inserting
                deleteAt += 1;
            }
            else if(destinationIndexPath.Row > sourceIndexPath.Row && sourceIndexPath.Section == destinationIndexPath.Section)
            {
                // add one to where we insert, because we haven't deleted the original yet
                insertAt += 1;
            }

            if (sourceIndexPath.Section == destinationIndexPath.Section)
            {
                headers[destinationIndexPath.Section].wordTableList.Insert(insertAt, item);
                headers[sourceIndexPath.Section].wordTableList.RemoveAt(deleteAt);
            }
            else if (sourceIndexPath.Section != destinationIndexPath.Section)
            {
                headers[destinationIndexPath.Section].wordTableList.Insert(insertAt, item);
                headers[sourceIndexPath.Section].wordTableList.RemoveAt(deleteAt);
            }

            db.ReinsertWords(headers[0].wordTableList, headers[1].wordTableList);

            GetWordsFromDatabase();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(HamburgerButton());

            //show words count in bottom toolbar
            SetWordsCountListenerTitle(wordTableList.Count());
        }

        #endregion

        #region Header

        [Export("tableView:viewForHeaderInSection:")]
        public UIView GetViewForHeader(UITableView tableView, nint section)
        {
            currSection = Convert.ToInt32(section);

            ExpandableHeaderView.headerHeight = headerHeight;
            ExpandableHeaderView header = new ExpandableHeaderView();

            headers[currSection].headerLink = header;

            HeaderImageSetup(currSection);

            header.Touched += () =>
            {
                ExpandCollapseHeader(Convert.ToInt32(section));
            };

            header.setup(headers[currSection].Title);

            return header;
        }

        [Export("numberOfSectionsInTableView:")]
        public nint NumberOfSections(UITableView tableView)
        {
            return headers.Count;
        }

        [Export("tableView:heightForHeaderInSection:")]
        public nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return headerHeight; //44
        }

        [Export("tableView:heightForRowAtIndexPath:")]
        public nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if (headers[indexPath.Section].expanded)
                return cellHeight; //60
            else
                return 0;
        }

        [Export("tableView:heightForFooterInSection:")]
        public nfloat GetHeightForFooter(UITableView tableView, nint section)
        {
            return 2;
        }

        #endregion

        #region Private Methods

        void GetWordsFromDatabase()
        {
            db = new DataBase(wordTableDbPath);

            wordTableList = db.GetWords();

            //show words count in bottom toolbar
            SetWordsCountListenerTitle(wordTableList.Count());

            if (WordsEqualZero() && this.IsViewLoaded)
            {
                ChangeEditWordButtonRole(switchTo: EditWordButtonRole.Edit);
            }

            int headerIndex = 0;
            while (headerIndex < headers.Count())
            {
                HeaderSetWordTable(index: headerIndex);

                headerIndex++;
            }

            UpdateIDs();
        }

        void HeaderSetWordTable(int index)
        {
            List<SQLWordTable> wordTableListTemporary = new List<SQLWordTable>();

            for (int i = 0; i < wordTableList.Count; i++)
            {
                if (wordTableList[i].headerIndex == index)
                {
                    wordTableListTemporary.Add(wordTableList[i]);
                }
            }

            headers[index].wordTableList = wordTableListTemporary;
        }

        void UpdateIDs()
        {
            IDs = new List<int>();

            foreach (var word in wordTableList)
            {
                IDs.Add(word.Id);
            }
        }

        void ChangeEditWordButtonRole(EditWordButtonRole switchTo)
        {
            switch (switchTo)
            {
                case EditWordButtonRole.Edit:
                    editWordButtonRole = EditWordButtonRole.Edit;

                    headerLock = false;

                    SetEditing(false, true);
                    EditButton.Title = "Edit";
                    break;

                case EditWordButtonRole.Done:
                    editWordButtonRole = EditWordButtonRole.Done;

                    ExpandAllHeaders();
                    headerLock = true;

                    SetEditing(true, true);
                    EditButton.Title = "Done";
                    break;
            }
        }

        void SetWordsCountListenerTitle(int wordsCount)
        {
            if (WordsCountListener == null)
            {
                return;
            }

            if (wordsCount == 1)
            {
                WordsCountListener.Title = String.Format("{0} word", wordsCount);
            }
            else
            {
                WordsCountListener.Title = String.Format("{0} words", wordsCount);
            }
        }

        bool WordsEqualZero()
        {
            if (wordTableList.Count == 0)
            {
                return true;
            }
            return false;
        }

        void SideMenuOpenAnim()
        {
            //CGAffineTransform transform = NavigationController.View.Transform====== 

            ////CGRect frame2 = View.Frame;

            ////frame2.Width -= 120;
            ////frame2.Height -= 120;

            //UIView.Animate(0.5f, () =>
            //{
            //    NavigationController.View.Transform.TransformRect(frame);
            //});
        }

        UIButton HamburgerButton()
        {

            UIButton button = UIButton.FromType(UIButtonType.Custom);
            UIImage image = UIImage.FromBundle("ic_menu");
            button.SetBackgroundImage(image, UIControlState.Normal);
            button.Frame = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);
            button.AddGestureRecognizer(new UITapGestureRecognizer(tap =>
            {
                SideMenuOpenAnim();
            }));

            return button;
        }

        #endregion

        #region Handlers

        partial void EditWords_Activated(UIBarButtonItem sender)
        {
            if (WordsEqualZero())
            {
                return;
            }

            if(editWordButtonRole == EditWordButtonRole.Edit)
            {
                ChangeEditWordButtonRole(EditWordButtonRole.Done);
            }
            else if(editWordButtonRole == EditWordButtonRole.Done)
            {
                ChangeEditWordButtonRole(EditWordButtonRole.Edit);
            }
        }

        partial void ButtonAdd_Activated(UIBarButtonItem sender)
        {
            ChangeEditWordButtonRole(switchTo: EditWordButtonRole.Edit);

            AddPopUpUIView AddWindow = new AddPopUpUIView();
            AddWindow.Closed += () =>
            {
                GetWordsFromDatabase();
                this.TableView.ReloadData();
            };
            View.AddSubview(AddWindow);
        }

        #endregion

        #region enums

        enum EditWordButtonRole
        {
            Edit,
            Done
        }

        #endregion
    }
}
// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace StudyHandy
{
    [Register ("TestTableView")]
    partial class MainTableView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem EditButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem WordsCountListener { get; set; }

        [Action ("ButtonAdd_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ButtonAdd_Activated (UIKit.UIBarButtonItem sender);

        [Action ("EditWords_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void EditWords_Activated (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
            if (EditButton != null) {
                EditButton.Dispose ();
                EditButton = null;
            }

            if (WordsCountListener != null) {
                WordsCountListener.Dispose ();
                WordsCountListener = null;
            }
        }
    }
}
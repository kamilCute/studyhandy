﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using StudyHandy.HeaderView;
using UIKit;

namespace StudyHandy
{
    public partial class MainTableView
    {
        void ExpandCollapseHeader(int section)
        {
            //headerLock set to true, when the EditMode enabled
            if (headerLock == true)
            {
                return;
            }

            var paths = new NSIndexPath[RowsInSection(TableView, section)];
            for (int i = 0; i < paths.Length; i++)
            {
                paths[i] = NSIndexPath.FromItemSection(i, section);
            }

            headers[section].expanded = !headers[section].expanded;

            HeaderImageSetup(section);

            TableView.ReloadRows(paths, UITableViewRowAnimation.Automatic);
        }

        void ExpandAllHeaders()
        {
            for (int index = 0; index < headers.Count(); index++)
            {
                headers[index].expanded = true;
                HeaderImageSetup(index);

                NSIndexPath[] paths = new NSIndexPath[RowsInSection(TableView, index)];
                for (int i = 0; i < paths.Length; i++)
                {
                    paths[i] = NSIndexPath.FromItemSection(i, index);
                }
                TableView.ReloadRows(paths, UITableViewRowAnimation.Automatic);
            }
        }

        void HeaderImageSetup(int section)
        {
            if (headers[section].expanded == true)
            {
                headers[section].headerLink.imageButton.SetImage(UIImage.FromBundle("ExpandedIcon"), UIControlState.Normal);
            }
            else
            {
                headers[section].headerLink.imageButton.SetImage(UIImage.FromBundle("CollapsedIcon"), UIControlState.Normal);
            }
        }
    }
}

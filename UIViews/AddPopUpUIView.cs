using CoreGraphics;
using Foundation;
using System;
using UIKit;
using StudyHandy.Extensions;
using System.IO;
using System.Threading.Tasks;
using ObjCRuntime;
using StudyHandy.Yandex.Translator;

namespace StudyHandy
{
    public class AddPopUpUIView : UIView
    {
        #region fields

        protected string wordTableDbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), SQLWordTable_DBPath.thisDBPath);

        YandexTranslatorAPI translator = new YandexTranslatorAPI();

        UILabel titleLabel;

        UILabel hiddenLabelExist;
        UILabel hiddenLabelEmpty;

        UITextField RussiaTxtField;
        UITextField EnglishTxtField;

        static float heightDividend; //делимое натуральной высоты девайса
        static int windowRightLeftMargin;
        static int windowTopMargin;
        static int iconSideMargin;
        static int iconTopMargin;
        static int txtFieldTopMargin;

        string UKText;
        string RUText;

        UIVisualEffectView effectView = new UIVisualEffectView(UIBlurEffect.FromStyle(UIBlurEffectStyle.Dark));

        UIImageView UKFlagImV;
        UIImageView RUFlagImV;

        UIButton btnOK;

        #endregion

        #region ctor

        public AddPopUpUIView()
        {
            InitLayout();

            //if touched outside of view - close it
            UITapGestureRecognizer tapGestureRecognizer = new UITapGestureRecognizer(target: this, action: new Selector("ClickedOutside"));
            tapGestureRecognizer.NumberOfTapsRequired = 1;

            effectView.UserInteractionEnabled = true;
            effectView.AddGestureRecognizer(tapGestureRecognizer);

            titleLabel = new UILabel
            {
                Text = "Add your word",
                TextColor = UIColor.Black,
                Font = Extension.getBold(23f)
            };
            AddSubview(titleLabel);

            UKFlagImV = new UIImageView();
            UKFlagImV.Layer.ShadowOffset = new CGSize(3, 3);
            UKFlagImV.Layer.ShadowOpacity = 0.2f;
            UKFlagImV.Image = UIImage.FromBundle("UKFlag");
            AddSubview(UKFlagImV);

            EnglishTxtField = new UITextField
            {
                Placeholder = "English",
                BorderStyle = UITextBorderStyle.RoundedRect
            };
            EnglishTxtField.AddTarget(this, new Selector("UKTextChanged"), UIControlEvent.EditingChanged);

            Extension.SetMaxLenght(EnglishTxtField, 10);

            AddSubview(EnglishTxtField);

            RUFlagImV = new UIImageView();
            RUFlagImV.Layer.ShadowOffset = new CGSize(3, 3);
            RUFlagImV.Layer.ShadowOpacity = 0.2f;
            RUFlagImV.Image = UIImage.FromBundle("RussiaFlag");
            AddSubview(RUFlagImV);

            RussiaTxtField = new UITextField
            {
                Placeholder = "Russian",
                BorderStyle = UITextBorderStyle.RoundedRect
            };

            Extension.SetMaxLenght(RussiaTxtField, 10);

            AddSubview(RussiaTxtField);

            btnOK = new UIButton(UIButtonType.System);
            btnOK.SetTitle("OK", UIControlState.Normal);
            btnOK.Layer.CornerRadius = 1;
            btnOK.TouchUpInside += (sender, e) =>
            {
                BtnOKClicked();
            };
            AddSubview(btnOK);

            hiddenLabelExist = new UILabel
            {
                Text = "Your vocabulary already contains given word",
                Alpha = 0,
                TextColor = UIColor.Red,
                Font = Extension.getRegular(17f)
            };
            AddSubview(hiddenLabelExist);

            hiddenLabelEmpty = new UILabel
            {
                Text = "Fill out all of the fields",
                Alpha = 0,
                TextColor = UIColor.Red,
                Font = Extension.getRegular(17f)
            };
            AddSubview(hiddenLabelEmpty);
        }

        #endregion

        #region Overriden Members

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Screen prop
            BackgroundColor = UIColor.FromRGB(250, 250, 250);

            var naturalWidth = (int)UIScreen.MainScreen.Bounds.Width;
            var naturalHeight = (int)UIScreen.MainScreen.Bounds.Height;
            var windowWidth = naturalWidth - 2 * windowRightLeftMargin;
            var windowHeight = naturalHeight / heightDividend;
            Frame = new CGRect(new CGPoint(windowRightLeftMargin, windowTopMargin), new CGSize(windowWidth, windowHeight));
            Layer.CornerRadius = 4;

            UIWindow window = UIApplication.SharedApplication.KeyWindow;
            effectView.Frame = window.Bounds;
            window.AddSubview(effectView);

            AnimateIn();

            window.AddSubview(this);

            titleLabel.SizeToFit();
            var x = (windowWidth / 2) - (titleLabel.Frame.Width) / 2;    //Отнимаем середину текста
            var y = windowHeight / 10;
            titleLabel.Frame = new CGRect(new CGPoint(x, y), new CGSize(titleLabel.Frame.Width, titleLabel.Frame.Height));

            var UKFlagSize = UKFlagImV.Image.Size;
            UKFlagImV.Frame = new CGRect(new CGPoint(Bounds.Left + iconSideMargin, titleLabel.Frame.Bottom + iconTopMargin), new CGSize(UKFlagSize.Width, UKFlagSize.Height));

            EnglishTxtField.Frame = new CGRect
            (new CGPoint(UKFlagImV.Frame.X - UKFlagSize.Width - 5.4 + 27, UKFlagImV.Frame.Bottom + txtFieldTopMargin), new CGSize(UKFlagImV.Bounds.Width * 2.2
                , UKFlagImV.Bounds.Height / 1.5));

            var RussiaFlagSize = RUFlagImV.Image.Size;
            RUFlagImV.Frame = new CGRect
            (new CGPoint(Bounds.Right - iconSideMargin - 54, titleLabel.Frame.Bottom + iconTopMargin), new CGSize(RussiaFlagSize.Width, RussiaFlagSize.Height));

            RussiaTxtField.Frame = new CGRect
            (new CGPoint(RUFlagImV.Frame.X - RussiaFlagSize.Width - 5.4 + 27, RUFlagImV.Frame.Bottom + txtFieldTopMargin), new CGSize(RUFlagImV.Bounds.Width * 2.2, RUFlagImV.Bounds.Height / 1.5));

            var btnWidth = 100;
            var btnHeight = 50;
            btnOK.Frame = new CGRect
            (new CGPoint((windowWidth / 2) - (btnWidth) / 2, RussiaTxtField.Frame.Bottom), new CGSize(btnWidth, btnHeight));

            hiddenLabelExist.SizeToFit();
            hiddenLabelExist.Frame = new CGRect
            (new CGPoint(btnOK.Bounds.X + ((Bounds.Width - hiddenLabelExist.Frame.Width) / 2), Bounds.Bottom + 10),
             new CGSize(hiddenLabelExist.Frame.Width, hiddenLabelExist.Frame.Height + 3));

            hiddenLabelEmpty.SizeToFit();
            hiddenLabelEmpty.Frame = new CGRect
            (new CGPoint(btnOK.Bounds.X + ((Bounds.Width - hiddenLabelEmpty.Frame.Width) / 2), Bounds.Bottom + 10),
             new CGSize(hiddenLabelEmpty.Frame.Width, hiddenLabelEmpty.Frame.Height + 3));
        }

        #endregion

        #region Private fields

        void InitLayout()
        {
            if (UIScreen.MainScreen.Bounds.Width < 414)
            {
                heightDividend = 3.3f;
                windowRightLeftMargin = 25;
                windowTopMargin = 120;
                iconSideMargin = 40;
                iconTopMargin = 20;
                txtFieldTopMargin = 25;
            }

            else
            {
                heightDividend = 4.2f;
                windowRightLeftMargin = 25;
                windowTopMargin = 120;
                iconSideMargin = 40;
                iconTopMargin = 35;
                txtFieldTopMargin = 25;
            }
        }

        async void BtnOKClicked()
        {
            DataBase db = new DataBase(wordTableDbPath);

            UKText = EnglishTxtField.Text;

            RUText = RussiaTxtField.Text;

            bool isFieldsEmpty = db.EmptyCheck(UKText, RUText);
            if (isFieldsEmpty) { LblNoticeAppear(targetLabel: hiddenLabelEmpty); return; }

            bool isExist = db.ExistCheck(UKText, RUText);
            if (isExist) { LblNoticeAppear(targetLabel: hiddenLabelExist); return; }

            db.InsertIntoDataBase(UKText, RUText);

            CloseView();
        }

        [Export("ClickedOutside")]
        void CloseView()
        {
            Animate(0.25, delegate
            {
                Transform = CGAffineTransform.MakeScale(1.3f, 1.3f);
                Alpha = 0;
                effectView.Alpha = 0;
            },
            delegate
            {
                effectView.RemoveFromSuperview();
                RemoveFromSuperview();
            });

            Closed?.Invoke();
        }

        void AnimateIn()
        {
            effectView.Alpha = 0;

            Alpha = 0;
            Transform = CGAffineTransform.MakeScale(1.3f, 1.3f);

            Animate(0.25, delegate
            {
                Transform = CGAffineTransform.MakeIdentity();
                Alpha = 1;
                effectView.Alpha = 1;
            });
        }

        async Task LblNoticeAppear(UILabel targetLabel)
        {
            targetLabel.Alpha = 1;

            await Task.Delay(2000);

            Animate(0.4, ()=> { targetLabel.Alpha = 0; });
        }

        [Export("UKTextChanged")]
        async void UKGettingTranslate()
        {
            string UKText = EnglishTxtField.Text;

            RussiaTxtField.Text = await translator.GetTranslate(UKText, LangCode.EngToRus);
        }

        #endregion

        public delegate void myDelegate();
        public event myDelegate Closed;
    }
}

﻿using SQLite;
using System.Collections.Generic;

namespace StudyHandy
{
    public class DataBase
    {
        #region Fields

        string pathToDatabase;

        SQLiteConnection connection;

        List<SQLWordTable> wordTableList;

        #endregion

        #region ctor

        public DataBase(string pathToDatabase)
        {
            this.pathToDatabase = pathToDatabase;

            connection = CreateConnection();
        }

        #endregion

        #region Private Methods

        private SQLiteConnection CreateConnection()
        {
            var SQLConnection = new SQLiteConnection(pathToDatabase);
            SQLConnection.CreateTable<SQLWordTable>();

            return SQLConnection;
        }

        #endregion

        #region Public Methods

        public void InsertIntoDataBase(string englishWord, string russianWord)
        {
            englishWord = englishWord.Replace(" ", string.Empty);
            russianWord = russianWord.Replace(" ", string.Empty);

            MainTableView.headers[0].wordTableList.Add(new SQLWordTable() { EnglishWord = englishWord, RussianWord = russianWord, headerIndex = 0 });

            ReinsertWords(MainTableView.headers[0].wordTableList, MainTableView.headers[1].wordTableList);
        }

        public void DeleteFromDataBase(int id)
        {
            lock (connection)
            {
                connection.Delete<SQLWordTable>(id);
            }
        }

        public bool ExistCheck(string UK, string RU)
        {
            wordTableList = GetWords();

            foreach (var x in wordTableList)
            {
                if (x.EnglishWord == UK || x.RussianWord == RU)
                {
                    return true;
                }
            }
            return false;
        }

        public bool EmptyCheck(string UK, string RU)
        {
            if (UK.Replace(" ", string.Empty) == string.Empty || RU.Replace(" ", string.Empty) == string.Empty)
            {
                return true;
            }

            return false;
        }

        public void ReinsertWords(List<SQLWordTable> headerWordsList1, List<SQLWordTable> headerWordsList2)
        {
            lock (connection)
            {
                connection.DeleteAll<SQLWordTable>();

                foreach (var item in headerWordsList1)
                {
                    connection.Insert(item);
                }

                foreach (var item in headerWordsList2)
                {
                    connection.Insert(item);
                }
            }
        }
        
        public List<SQLWordTable> GetWords()
        {
            wordTableList = new List<SQLWordTable>();

            var query = connection.Table<SQLWordTable>();

            foreach (var sqlWord in query)
            {
                wordTableList.Add(sqlWord);
            }
            return wordTableList;
        }

        #endregion
    }
}

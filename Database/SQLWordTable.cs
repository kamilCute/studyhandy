﻿using SQLite;

namespace StudyHandy
{
    public class SQLWordTable
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(12)]
        public string EnglishWord { get; set; }

        [MaxLength(12)]
        public string RussianWord { get; set; }

        public int headerIndex { get; set; }
    }

    public static class SQLWordTable_DBPath
    {
        public static string thisDBPath = "Studyhandy.db";
    }
}
